package com.example.employeescatalogue;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.employeescatalogue.data.model.Employee;
import com.example.employeescatalogue.data.source.CatalogueDataSource;
import com.example.employeescatalogue.databinding.ActivityCatalogueBinding;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class CatalogueActivity extends AppCompatActivity implements CatalogueContract.View, View.OnClickListener {
    private CatalogueAdapter adapter;
    private ActivityCatalogueBinding view;
    private CataloguePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = DataBindingUtil.setContentView(this, R.layout.activity_catalogue);
        new CataloguePresenter(this, CatalogueDataSource.getInstance()).start();
        view.buttonSave.setOnClickListener(this);
    }

    @Override
    public void setPresenter(CataloguePresenter cataloguePresenter) {
        this.presenter = cataloguePresenter;
    }

    @Override
    public void setAdapter(List<Employee> employeeList) {
        adapter = new CatalogueAdapter(employeeList, this);
        view.recycler.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        presenter.setUpdatedDataIntoFile(adapter.getUpdatedList());
    }
}
