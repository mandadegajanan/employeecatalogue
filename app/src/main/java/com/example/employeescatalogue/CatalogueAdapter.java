package com.example.employeescatalogue;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.employeescatalogue.data.model.Employee;
import com.example.employeescatalogue.databinding.EmployeeCatalogueItemBinding;

import java.util.List;

public class CatalogueAdapter extends RecyclerView.Adapter<CatalogueAdapter.ViewHolder> {

    private List<Employee> list;
    private Context context;

    CatalogueAdapter(List<Employee> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public CatalogueAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View statusContainer = LayoutInflater.from(parent.getContext()).inflate(R.layout.employee_catalogue_item, parent, false);
        return new ViewHolder(statusContainer);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Employee status = list.get(position);
        ArrayAdapter<Integer> arrayAdapter = new ArrayAdapter<>(
                context, android.R.layout.simple_spinner_item, status.getAge());
        holder.spinner.setAdapter(arrayAdapter);
        holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                status.setSelectedAge(status.getAge().get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        holder.isPhysicalHandicappedCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                status.setPhysicallyHandicapped(isChecked);
            }
        });
        holder.bindUser(status);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    List<Employee> getUpdatedList() {
        return list;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private EmployeeCatalogueItemBinding binding;
        private Spinner spinner;
        private CheckBox isPhysicalHandicappedCheck;

        ViewHolder(View itemView) {
            super(itemView);
            spinner = itemView.findViewById(R.id.spinner_age);
            isPhysicalHandicappedCheck = itemView.findViewById(R.id.cb_physical_handicapped);
            binding = DataBindingUtil.bind(itemView);
        }

        void bindUser(Employee user) {
            binding.setEmployee(user);
        }
    }
}