package com.example.employeescatalogue;

import com.example.employeescatalogue.data.model.Employee;

import java.util.List;

public interface CatalogueContract {

    interface View {

        void setPresenter(CataloguePresenter cataloguePresenter);

        void setAdapter(List<Employee> employeeList);
    }

    interface Presenter {
        void start();

        void setUpdatedDataIntoFile(List<Employee> updatedList);
    }
}
