package com.example.employeescatalogue;

import com.example.employeescatalogue.data.CatalogueRepository;
import com.example.employeescatalogue.data.model.Employee;
import com.example.employeescatalogue.data.source.CatalogueDataSource;

import java.util.List;

public class CataloguePresenter implements CatalogueContract.Presenter {
    private final CatalogueContract.View view;
    private CatalogueRepository catalogueRepository;

    CataloguePresenter(CatalogueContract.View view,
                       CatalogueRepository catalogueRepository) {
        this.view = view;
        this.catalogueRepository = catalogueRepository;
        view.setPresenter(this);
    }

    @Override
    public void start() {
        catalogueRepository.getEmployees(new CatalogueDataSource.CallBack() {
            @Override
            public void onDataReceive(List<Employee> employeeList) {
                view.setAdapter(employeeList);
            }
        });
    }

    @Override
    public void setUpdatedDataIntoFile(List<Employee> updatedList) {
        catalogueRepository.setDataIntoFile(updatedList);
    }
}
