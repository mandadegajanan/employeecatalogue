package com.example.employeescatalogue.data;

import com.example.employeescatalogue.data.model.Employee;
import com.example.employeescatalogue.data.source.CatalogueDataSource;

import java.util.List;

public interface CatalogueRepository {
    void getEmployees(CatalogueDataSource.CallBack callback);

    void setDataIntoFile(List<Employee> updatedList);
}
