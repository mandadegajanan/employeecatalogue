package com.example.employeescatalogue.data.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Employee {
    public static final String NAME = "name";
    public static final String IS_PHYSICALLY_HANDICAPPED = "isPhysicallyHandicapped";
    public static final String AGE = "age";

    private String name;
    private boolean isPhysicallyHandicapped;
    private List<Integer> age = new ArrayList<>();
    private Integer selectedAge;

    public Employee(JSONObject jsonObject) {
        if (jsonObject != null) {
            setName(jsonObject.optString(NAME));
            setPhysicallyHandicapped(jsonObject.optBoolean(
                    IS_PHYSICALLY_HANDICAPPED, false));
            JSONArray ageArray = jsonObject.optJSONArray(AGE);
            for (int i = 0; i < Objects.requireNonNull(ageArray).length(); i++) {
                age.add(ageArray.optInt(i));
            }
        }
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public boolean isPhysicallyHandicapped() {
        return isPhysicallyHandicapped;
    }

    public void setPhysicallyHandicapped(boolean physicallyHandicapped) {
        isPhysicallyHandicapped = physicallyHandicapped;
    }

    public List<Integer> getAge() {
        return age;
    }

    public Integer getSelectedAge() {
        return selectedAge;
    }

    public void setSelectedAge(Integer selectedAge) {
        this.selectedAge = selectedAge;
    }
}
