package com.example.employeescatalogue.data.source;

import com.example.employeescatalogue.data.CatalogueRepository;
import com.example.employeescatalogue.data.model.Employee;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CatalogueDataSource implements CatalogueRepository {

    private static final String EMPLOYEE_CATALOGUE = "employee_catalogue";
    private static CatalogueRepository instance = null;

    public static CatalogueRepository getInstance() {
        synchronized (CatalogueDataSource.class) {
            if (instance == null) {
                instance = new CatalogueDataSource();
            }
            return instance;
        }
    }

    @Override
    public void getEmployees(CallBack callback) {
        String json = "{\"employee_catalogue\":[{\"name\":\"Rachel Sheldon\",\"isPhysicallyHandicapped\":false,\"age\":[30,31,32,33,34,35,36]},{\"name\":\"James Petter\",\"isPhysicallyHandicapped\":false,\"age\":[30,31,32,33,34,35,36]},{\"name\":\"Richard Pett\",\"isPhysicallyHandicapped\":false,\"age\":[30,31,32,33,34,35,36]},{\"name\":\"Mark Clegg\",\"isPhysicallyHandicapped\":false,\"age\":[30,31,32,33,34,35,36]},{\"name\":\"Joseph Bond\",\"isPhysicallyHandicapped\":false,\"age\":[30,31,32,33,34,35,36]},{\"name\":\"Clare Blake\",\"isPhysicallyHandicapped\":false,\"age\":[30,31,32,33,34,35,36]},{\"name\":\"Bradley Willlium\",\"isPhysicallyHandicapped\":true,\"age\":[30,31,32,33,34,35,36]},{\"name\":\"Steve Smith\",\"isPhysicallyHandicapped\":true,\"age\":[30,31,32,33,34,35,36]},{\"name\":\"Harry Harrison\",\"isPhysicalyHandicapped\":false,\"age\":[30,31,32,33,34,35,36]}]}";
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            JSONArray array = jsonObject.optJSONArray(EMPLOYEE_CATALOGUE);
            List<Employee> employeeList = new ArrayList<>();
            for (int j = 0; j < Objects.requireNonNull(array).length(); j++) {
                employeeList.add(new Employee(array.optJSONObject(j)));
            }
            callback.onDataReceive(employeeList);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void setDataIntoFile(List<Employee> updatedList) {
        try {
            JSONObject mainObject = new JSONObject();
            JSONArray array = new JSONArray();
            for (Employee employee : updatedList) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Employee.NAME, employee.getName());
                jsonObject.put(Employee.IS_PHYSICALLY_HANDICAPPED, employee.isPhysicallyHandicapped());
                jsonObject.put(Employee.AGE, employee.getSelectedAge());
                array.put(jsonObject);
            }
            mainObject.put(EMPLOYEE_CATALOGUE, array);
            File file = new File("data/data/com.example.employeescatalogue/test.txt");
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            FileWriter writer = new FileWriter(file);
            writer.append(mainObject.toString());
            writer.flush();
            writer.close();
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    public interface CallBack {
        void onDataReceive(List<Employee> employeeList);
    }
}
